# Gutenberg-Richter Parameters; Spatial Distribution

**abMapper2** is a small application that runs in MATLAB.

It uses a given earthquake catalog and it estimates the a-value and b-value in 2D area.

Dowload the repository and open it in a MATLAB environment, then execute the file abMapper2.mlapp

The implemented catalog can be found inside SurESP_Depurado.mat and it is called "catalog".

This is a declustered catalog of southern Spain (including historical events and instrumental until 2011).



There is an option for you if you want to use your own catalog too.

For example if your catalog is in Excel, you can load it to MATLAB by typing the command:

`catalog=xlsread("your excel filename");`

And you can save it into a workspace with name "myCatalog" by typing the command:

`save('myCatalog','catalog')`

The Interface and an example of b-value map can be found inside AppFig folder