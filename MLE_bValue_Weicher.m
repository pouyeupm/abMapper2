function b=MLE_bValue_Weicher(compTime, magnitude, number,Nmin)
if size(magnitude,1)>2
        %check total number of events
        N=sum(number);
        if N>Nmin    
            minb=0.1; maxb=3.0;
            incb=0.05;
            l=(maxb-minb)/incb;

            b=minb;
            for i=1:l
                b=b+incb;
                beta(i)=b*log(10);
                f=exp(magnitude*-1*beta(i));
                dlnL(i)=-1*sum(magnitude.*number)+N*(sum((compTime.*magnitude).*(f))/(sum(compTime.*(f))));
            end

            dlnLfit=fit(beta', dlnL','linearinterp');
            dlnLfunc=@(bet) feval(dlnLfit,bet);
            beta=fzero(dlnLfunc,minb*log(10));
            f=exp(magnitude*-1*beta);    
            d2lnL=N*(((sum((compTime.*magnitude).*(f))^2)/(sum(compTime.*(f))^2))-((sum((compTime.*magnitude.*magnitude).*(f)))/(sum(compTime.*(f)))));
            if d2lnL>=0
                b=0;
            else
                b=beta/log(10);
            end
        else
            b=-150; %less than Nmin events exist
        end
else
    b=-200; % less than 2 magnitude values
end
end