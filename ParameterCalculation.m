function [aMatrix,rateMatrix,magRange,bMatrix]=ParameterCalculation(pointTable,Nmin,Mcut,varargin)
        if isempty(varargin)==1
            Mmax=7.0;
        else
            Mmax=varargin{1};
        end
        nx=size(pointTable,2);
        ny=size(pointTable,1);
        
        %modifying poinTable to a table with Mcut < mag < Mmax
        for i=1:nx
            for j=1:ny
                table=pointTable{j,i};
                if isempty(table)==0 %if this cell has data go on
                    index1=find(table(:,1)>Mcut-0.05,1);
                    index2=find(table(:,1)< Mmax,1,'last');
                    table(:,5)=table(:,5)-(table(index2,5)-table(index2,4));
                    table=table(index1:index2,:);
                    magRange{j,i}=table;
                end
            end
        end                    

        % matrix of yearly rate of mag>=Mcut for each grid point
        rateMatrix=zeros(ny,nx);
        for i=1:nx
            for j=1:ny
                table=magRange{j,i};
                if isempty(table)==1 %if this cell has no data
                    rateMatrix(j,i)=-100; % -100 stands for no events
                elseif sum(table(:,2))<0.5
                    rateMatrix(j,i)=-100; % -100 stands for no events
                else
                    rateMatrix(j,i)=table(1,5);
                end                
            end
        end
        
        % matrix of yearly rate of mag>=Mcut for each grid point
        finYear=2011;
        for i=1:nx
            for j=1:ny
                table=magRange{j,i};
                if isempty(table)==1 %if this cell has no data
                    bMatrix(j,i)=-100; % -100 stands for no events
                    aMatrix(j,i)=-100;
                elseif sum(table(:,2))<0.5
                    bMatrix(j,i)=-100;
                    aMatrix(j,i)=-100;
                else
                    m=table(:,1);
                    n=table(:,2);                           
                    t=finYear-table(:,3);
                    N=table(1,5);
                    bMatrix(j,i)=MLE_bValue_Weicher(t,m,n,Nmin); 
                    if bMatrix(j,i)==-150
                        aMatrix(j,i)=-150;
                    elseif bMatrix(j,i)==-200
                        aMatrix(j,i)=-200;
                    else
                        aMatrix(j,i)=log10(rateMatrix(j,i)*power(10,bMatrix(j,i)*Mcut));
                end
            end
        end
end
