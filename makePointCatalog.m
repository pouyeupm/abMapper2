%**************************************************************************
% This function needs a catalog in Zmap format----> catalog
% It need to grid points along latitude  ---->ygrid
%                        along longitude ---->xgrid
% Then for each gridpoints search for events insid "catalog" and with distance <=r
% Each cell of the pointCat represent a gridpoint and have a subset of "catalog"
%
%
% Then for all subset of "catalog" prepars a table including 
%                                      magnitude increments
%                                      number of each Mag
%                                      completitude Year (Sur de Spain)
%                                      yearly rate of each Mag
%                                      cumulative rate of each Mag
% ATTENTION: absent magnitudes are included! For Exclude them put varargin
% equel to 'YES'
% magnitude increments starts from mMin given by user
%**************************************************************************
% Pouye Yazdi
% 20 July 2017
%**************************************************************************

function [pointCat,pointTable]=makePointCatalog(mMin,r,zmapdatafile,xgrid,ygrid,varargin)
load(zmapdatafile);
pointCat=cell(size(ygrid,2),size(xgrid,2));
pointTable=cell(size(ygrid,2),size(xgrid,2));
finYear=2011;
new=catalog;

            for i=1:size(xgrid,2)
                xa0=xgrid(i);
%%                
                for j=1:size(ygrid,2)
                    ya0=ygrid(j);
                    l = sqrt(((catalog(:,1)-xa0)*cos(pi/180*ya0)*91).^2 + ((catalog(:,2)-ya0)*111).^2) ; % distances
                    [s,is]=sort(l);
                    isMax=find(s>r,1)-1;
                    selected_events_index=is(1:isMax);
                    selected_events_index=sort(selected_events_index);
                    %Catalog for each point in GRID
                    pointCat{j,i}=new(selected_events_index,:);
                    if isempty(pointCat{j,i})==0
                        %**************************************************************************
                        magIncremets=[mMin:0.1:7.0]';
                        S=size(magIncremets,1);
                        nMag=zeros(S,1); %number of events with Mag

                        magList=new(selected_events_index,6);
                        m=sort(magList);

                        for k=1:S-1
                            nMag(k)=sum(m>=magIncremets(k))-sum(m>=magIncremets(k+1));
                        end
                        k=S;                
                        nMag(k)=sum(m>=magIncremets(k)); 
                        if isempty(varargin)==0
                            [s,is]=sort(nMag); 
                            n0=find(s>0,1);
                            selected_events_index=is(n0:end);
                            selected_events_index=sort(selected_events_index);
                            nMag=nMag(selected_events_index,1);
                            magIncremets=magIncremets(selected_events_index,1);
                        end
                        matrix=[magIncremets,nMag];
                        for f=1:size(matrix,1)
                            if matrix(f,1)<3.5
                                matrix(f,3)=1978;
                            else if matrix(f,1)<4.0
                                    matrix(f,3)=1975;
                                else if matrix(f,1)<4.5
                                        matrix(f,3)=1908;
                                    else if matrix(f,1)<5.0
                                            matrix(f,3)=1883;
                                        else if matrix(f,1)<5.5
                                                matrix(f,3)=1800;
                                            else if matrix(f,1)<6.0
                                                        matrix(f,3)=1520;
                                                    else 
                                                        matrix(f,3)=1048;
                                                    end
                                            end
                                        end
                                    end
                                end
                            end
                        end

                        matrix(:,4)=-1*matrix(:,2)./(matrix(:,3)-finYear);

                        for h=1:size(magIncremets,1)
                            matrix(h,5)=sum(matrix(h:end,4));
                        end
                        %Magnitude frequency for each point in GRID
                        pointTable{j,i}=matrix;
                    end %if pointCat{j,i} was empty then there will be no pointTable or it will be empty as well
                end
                %%            
            end
            
end

