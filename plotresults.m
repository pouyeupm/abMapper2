function plotresults(xgrid,ygrid,radius,aValue,bValue,rate,Nmin,Mcut,figname,varargin)   
set(0,'DefaultLegendAutoUpdate','off')    
filename=sprintf('N%dR%d_%s',Nmin,radius,figname);

    % Make color range for b-value
    value_colorset=[1,0.89,0.89;
        0.98,0.58,0.60;
        0.85,0.11,0.16;
        0.5,0.04,0.04];
    n = 9; % size of new color map
    m = size(value_colorset,1); %will be 5
    t0 =linspace(0,1,m)';
    t = linspace(0,1,n)';
    r = interp1(t0,value_colorset(:,1),t);
    g = interp1(t0,value_colorset(:,2),t);
    b = interp1(t0,value_colorset(:,3),t);
    rgb_colorset = [r,g,b];
    %% *******************************
    %  b-value map
    h(1) = figure; 
    imagesc(xgrid,ygrid,bValue);
    set(gca,'Ydir','normal');
    ax = gca;
    ax.FontSize = 14; 
    xlabel('longitude','FontSize',13);
    ylabel('latitude','FontSize',13);
    title(sprintf('b-value map for m_{c}= %3.1f\n and a minimum of %d events inside a radius of %d km',Mcut, Nmin, radius));
    colormap(rgb_colorset);
    caxis([0.65,1.55]);
    colorbar;
    %% *******************************
        hold on
        new=bValue;
        [row,col]=find(bValue<0.65&bValue>0 );
        scatter(xgrid(col),ygrid(row),25,'s','MarkerEdgeColor',[0.67 0.67 0.67],'MarkerFaceColor',[1 1 1],'LineWidth',0.2)
        [row,col]=find(bValue<-140);
        scatter(xgrid(col),ygrid(row),25,[0.67 0.67 0.67],'filled','s')
        legend('b-value < 0.65','few data'); 

        if isempty(varargin)==0
            load(varargin{1});
            plot(coastline(:,1),coastline(:,2),'k','LineWidth',1.5);
            xlim=[-6,1];
            ylim=[35,40];
            QAFifaults=sortrows(QAFifaults,1);
            for n=0:max(QAFifaults(:,1))
                list=find(QAFifaults(:,1)==n);
                fault=QAFifaults(list,:);
                fault=sortrows(fault,2);    
                plot(fault(:,2),fault(:,3),'Color',[0 0.24 0.55],'LineWidth',3)
                plot(fault(:,2),fault(:,3),'Color',[1 1 0.4],'LineWidth',2)
            end 
        end
        hold off 
    %% *******************************
    % Make color range for a-value
    value_colorset=[0.89,0.89,1;
        0.5,0.91,0.94
        0.27,0.77,0.81;
        0.12,0.57,0.65];
    n = 7; % size of new color map
    m = size(value_colorset,1); 
    t0 = linspace(0,1,m)';
    t = linspace(0,1,n)';
    r = interp1(t0,value_colorset(:,1),t);
    g = interp1(t0,value_colorset(:,2),t);
    b = interp1(t0,value_colorset(:,3),t);
    rgb_colorset = [r,g,b];
    %% *******************************
    %  a-value map
    h(2) = figure; 
    imagesc(xgrid,ygrid,aValue);
    set(gca,'Ydir','normal');
    ax = gca;
    ax.FontSize = 14; 
    xlabel('longitude','FontSize',13);
    ylabel('latitude','FontSize',13);
    title(sprintf('a-value map for m_{c}= %3.1f\n and a minimum of %d events inside a radius of %d km',Mcut, Nmin, radius));
    colormap(rgb_colorset);
    caxis([1,4.5]);
    colorbar;
    %% *******************************
        hold on
        new=aValue;
        [row,col]=find(aValue<1&aValue>0 );
        scatter(xgrid(col),ygrid(row),25,'s','MarkerEdgeColor',[0.67 0.67 0.67],'MarkerFaceColor',[1 1 1],'LineWidth',0.2)
        [row,col]=find(aValue<-140);
        scatter(xgrid(col),ygrid(row),25,[0.67 0.67 0.67],'filled','s')
        legend('a-value < 1.0','few data'); 

        if isempty(varargin)==0
            load(varargin{1});
            plot(coastline(:,1),coastline(:,2),'k','LineWidth',1.5);
            xlim=[-6,1];
            ylim=[35,40];
            QAFifaults=sortrows(QAFifaults,1);
            for n=0:max(QAFifaults(:,1))
                list=find(QAFifaults(:,1)==n);
                fault=QAFifaults(list,:);
                fault=sortrows(fault,2);    
                plot(fault(:,2),fault(:,3),'Color',[0 0.24 0.55],'LineWidth',3)
                plot(fault(:,2),fault(:,3),'Color',[1 1 0.4],'LineWidth',2)
            end 
        end
        hold off

    %% *******************************
    A=1/(pi*(radius^2));
    value=rate*A;
    maxValue=(max(max(value)));
    p=0;
    while maxValue<1
        maxValue=maxValue*10;
        p=p+1;
    end
    Inc=(10^(-1*(p+1)))/2;
    maxlim=round(maxValue)*(10^(-1*p))+Inc;
    minlim=Inc;
    % Make color range for rate
    value_colorset=[0.85,1,0.85;
        0.23,0.79,0.23;
        0.05,0.4,0.05];
    n = (maxlim-minlim)*(10^(p+1)); % size of new color map
    m = size(value_colorset,1); %will be 5
    t0 = linspace(0,1,m)';
    t = linspace(0,1,n)';
    r = interp1(t0,value_colorset(:,1),t);
    g = interp1(t0,value_colorset(:,2),t);
    b = interp1(t0,value_colorset(:,3),t);
    rgb_colorset = [r,g,b];
    %% *******************************       
    h(3)=figure;
    imagesc(xgrid,ygrid,value);
    set(gca,'Ydir','normal');
    ax = gca;
    ax.FontSize = 14; 
    xlabel('longitude','FontSize',13);
    ylabel('latitude','FontSize',13);
    title(sprintf('Annual N(m %s %3.1f) / %sR^{2}, for R=%d km','\geq',Mcut,'\pi',radius));
    colormap(rgb_colorset);
    caxis([minlim,maxlim]);
    colorbar;
    %% *******************************
    hold on
    new=value;
    [row,col]=find(value<minlim);
    scatter(xgrid(col),ygrid(row),25,[0.67 0.67 0.67],'filled','s')
    legend(sprintf('value less than %6.6f',minlim)); 

    if isempty(varargin)==0
        load(varargin{1});
        plot(coastline(:,1),coastline(:,2),'k','LineWidth',1.5);
        xlim=[-6,1];
        ylim=[35,40];
        QAFifaults=sortrows(QAFifaults,1);
        for n=0:max(QAFifaults(:,1))
            list=find(QAFifaults(:,1)==n);
            fault=QAFifaults(list,:);
            fault=sortrows(fault,2);    
            plot(fault(:,2),fault(:,3),'Color',[0 0.24 0.55],'LineWidth',3)
            plot(fault(:,2),fault(:,3),'Color',[1 1 0.4],'LineWidth',2)
        end 
    end

    savefig(h,filename)
    close(h) 
end